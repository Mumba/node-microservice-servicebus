/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const retry = require('servicebus-retry');
import {Config} from "mumba-config";
import {DiContainer} from "dicontainer";
import {bus, Bus} from "servicebus";

/**
 * ServiceBus module.
 */
export class MicroserviceServiceBusModule {
	/**
	 * Register the module assets in the DI container.
	 *
	 * @param {DiContainer} container
	 */
	static register(container: DiContainer) {
		container.factory('serviceBus', (config: Config) => {
			let serviceBus: Bus = bus(config.get('amqp'));

			serviceBus.use(retry({
				store: new retry.MemoryStore()
			}));
			serviceBus.use(serviceBus.correlate());
			serviceBus.use(serviceBus.package());

			return serviceBus;
		});
	}
}

