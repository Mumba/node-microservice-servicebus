// Generated by typings
// Source: node_modules/mumba-typedef-servicebus/index.d.ts
declare module "servicebus" {
	export class Bus {
		correlate: any;
		messageDomain: any;
		logger: any;
		package: any;
		retry: any;

		constructor(options: any);

		use(middleware: any): this;

		listen(queueName: string, options: any, callback: Function): void;
		listen(queueName: string, callback: Function): void;

		unlisten(queueName: string, options?: any): any; // TODO returns a Queue
		destroyListener(queueName: string, options?: any): void;

		// setOptions(queueName, options)
		send(queueName: string, message: any, options?: any, cb?: Function): void;

		close(): void;
	}

	export function bus(options?: any, implOpts?: any): Bus;
}

declare module "servicebus-retry" {
	export interface BusRetryEvent {
		// handle is optional so Entity events can be implement or extend from this
		handle?: {
			ack(callback?: (err?: Error) => void): void,
			acknowledge(callback?: (err?: Error) => void): void,
			reject(callback?: (err?: Error) => void): void
		}
	}
}
