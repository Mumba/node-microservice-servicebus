/**
 * Test configuration.
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

let testConfig = {
	'amqp': {
		url: 'amqp://rabbitmq'
	}
};

export default testConfig;
