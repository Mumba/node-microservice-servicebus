/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from "assert";
import {Bus} from "servicebus";
import {DiContainer} from "dicontainer";
import {MicroserviceServiceBusModule} from '../../src/index';
import {createConfig} from "../bootstrap";

describe('MicroserviceServiceBusModule integration tests', () => {
	let serviceBus_: Bus;

	beforeEach(() => {
		let container: DiContainer = new Sandal();

		container.object('config', createConfig());
		MicroserviceServiceBusModule.register(container);

		return new Promise((resolve, reject) => container.resolve((err: Error, serviceBus: Bus) => {
			if (err) {
				return reject(err);
			}

			serviceBus_ = serviceBus;
			resolve();
		}));
	});

	afterEach((done) => {
		setTimeout(() => {
			serviceBus_.close();
			done();
		}, 100);
	});

	it('should add the cid and package the event', (done) => {
		serviceBus_.listen('test-queue', (event: any) => {
			assert(event.data.cid, 'should have a cid and data');
			done();
		});

		serviceBus_.send('test-queue', { foo: 'bar' });
	});
});
