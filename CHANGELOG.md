# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.2.1] 19 Aug 2016
## [0.2.0]
- Added correlate and package middleware. Fixed but in configuration of retry.

## [0.1.1] 19 Aug 2016
## [0.1.0] 19 Aug 2016
- Initial release
