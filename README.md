[![build status](https://gitlab.com/Mumba/node-microservice-servicebus/badges/master/build.svg)](https://gitlab.com/Mumba/node-microservice-servicebus/commits/master)
# Mumba Microservice ServiceBus

Provide Service Bus (via RabbitMQ) support for a Microservice.

## Configuration

This module requires that a `Config` object has been registered as `config`.

Property | Type | Description
--- | :---: | ---
`amqp` | `object` | A dictionary of options required for RabbitMQ.
`amqp:url` | `string` | The URL of the RabbitMQ server (for example `amqp://rabbitmq`). 

## DI Container

This module adds the following container artifacts if they don't already exist.

Name | Type | Description
--- | :---: | ---
`serviceBus` | `Bus` | An instance of a [`servicebus`](https://github.com/mateodelnorte/servicebus) object.

## Installation

```sh
$ npm install --save mumba-microservice-servicebus
$ typings install --save dicontainer=npm:mumba-typedef-dicontainer
```

## Examples

TODO

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm run docker:up
$ npm install
$ npm test
$ npm docker:down
```

## People

The original author of _Mumba Microservice Service Bus_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-microservice-servicebus/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

